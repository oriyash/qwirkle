from termcolor import colored
from random import randint
from GameBoard import *
import os

def getInt(string):
    numberIn = input(string)
    try:
        return int(numberIn)
    except ValueError:
        print(colored('Invalid input. Try again.', COLORS.RED ))
        return getInt(string)

def clearScreen():
    if os.name == 'nt':
        os.system('cls')
    elif os.name == 'posix':
        os.system('clear')
    else:
        pass

def pause():
    if os.name == 'nt':
        os.system('pause')
    else:
        pass 

def newBag(c,s,r):
    bag=[]

    shapes_All = [
        SHAPES.CIRCLE,
        SHAPES.DIAMOND,
        SHAPES.SPARKLE,
        SHAPES.SQUARE,
        SHAPES.STAR,
        SHAPES.TRIANGLE
    ]

    colors_All = [
        COLORS.BLUE,
        COLORS.CYAN,
        COLORS.GREEN,
        COLORS.MAGENTA,
        COLORS.RED,
        COLORS.YELLOW 
    ]
    colors=colors_All[:c]
    shapes=shapes_All[:s]
    for i in range(r):
        for color in colors:
            for shape in shapes:
                bag.append(piece(color,shape))
    return bag

def switchPlayer(numberOfPlayers, whoPlays):
    if whoPlays < numberOfPlayers-1:
        return whoPlays + 1
    else:
        return 0

def start(grid,player,bag): 
    tile=getInt('Enter the number of the tile:') 
    grid[1][1]=player.tiles[tile-1] 
    player.tiles.pop(tile-1) 
    tileIndex = randint(0,len(bag)-1) 
    player.tiles.append(bag[tileIndex])
    bag.pop(tileIndex) 

def play(grid,player,bag): 
    clearScreen()
    print(colored("IT'S {}'S TURN".format(player.name), 'blue'))
    print('Number of Tiles left: ' + str(len(bag)))
    player.showTiles()
    grid.display()
    print('--> 0 to finish your turn \n --> -1 to trade tiles')
    tile=getInt('Enter the number of the tile: ')
    if tile==-1:
        player.trade_Hands(bag)
        return
    while True:
        clearScreen()
        print(colored("IT'S {}'S TURN".format(player.name), 'blue'))
        print('Number of Tiles left: ' + str(len(bag)))

        player.showTiles()
        grid.display()
        
        if tile!=0:
            print('--> 0 to finish your turn ')
            tile=getInt('Enter the number of the tile: ')
         
        if tile > 0:
            rows=getInt('In which row do you want to play? --> ') 
            columns=getInt('In which column do you want to play? --> ')
            rows=rows-1
            columns=columns-1
            if grid.checkRange(rows,columns):
                if grid.checkPlay(rows,columns,player.tiles[tile-1]):
                    grid.board[rows][columns]=player.tiles[tile-1] 

                    if(columns==0):
                        for i in range(0,len(grid.board)):       
                                grid.board[i].insert(0,piece(COLORS.WHITE, SHAPES.EMPTY))
                    elif(columns==len(grid.board[0])-1):
                        for i in range(0,len(grid.board)):       
                                grid.board[i].append(piece(COLORS.WHITE, SHAPES.EMPTY))
                    elif(rows==0):
                        grid.board.insert(0,empty_rows(grid.board))
                    elif(rows==len(grid.board)-1):
                        grid.board.append(empty_rows(grid.board))
                    player.tiles.pop(tile-1)
                    tileIndex = randint(0,len(bag)-1) 
                    if len(bag)>0:
                        player.tiles.append(bag[tileIndex])
                        bag.pop(tileIndex)
                    
                else:
                    clearScreen()
                    print(colored("IT'S {}'S TURN".format(player.name), 'blue'))
                    player.showTiles()
                    print(colored('Invalid play. Try again.', COLORS.RED))
                    grid.display()
            else:
                clearScreen()
                print(colored("IT'S {}'S TURN".format(player.name), 'blue'))
                player.showTiles()
                print(colored('Invalid play. Try again.', COLORS.RED))
                grid.display()
        
        else:
            break
        

def empty_rows(grid):
    row=[] 
    if(len(grid[0])):
        for x in range(len(grid[0])) :
            row.append(piece(COLORS.WHITE, SHAPES.EMPTY))
        return row 
    

def setBag():    
    numColor=getInt('Enter the number of colors (betwwen 3 and 6): ')
    while numColor<3 or numColor>6:
        print(colored('Invalid Number of colors try again', 'red'))
        numColor=getInt('Enter the number of colors (betwwen 3 and 6): ')
    
    numShapes=getInt('Enter the number of shapes(betwwen 3 and 6): ')
    while numShapes<3 or numShapes>6:
        print(colored('Invalid Number of shapes try again','red'))
        numShapes=getInt('Enter the number of colors (betwwen 3 and 6): ')
    
    numTiles=getInt('Enter the number of identical Tiles (at least 2): ')
    while numTiles<2:
        print(colored('Invalid number of identical tiles try again', 'red'))
        numTiles=getInt('Enter the number of identical Tiles (at least 2): ')
    
    return newBag(numColor,numShapes,numTiles) 

def giveTiles(playerList, allTiles):
    for player in playerList:
        for i in range(6):
            tileIndex = randint(0,len(allTiles)-1)
            tile = allTiles[tileIndex]
            player.addTile(tile)
            del allTiles[tileIndex]