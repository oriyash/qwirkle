from termcolor import colored
from GameBoard import *

class gameboard:
    
    def __init__(self):
        self.board = [
            [piece(COLORS.WHITE, SHAPES.EMPTY),piece(COLORS.WHITE, SHAPES.EMPTY),piece(COLORS.WHITE, SHAPES.EMPTY)],
            [piece(COLORS.WHITE, SHAPES.EMPTY),piece(COLORS.WHITE, SHAPES.EMPTY),piece(COLORS.WHITE, SHAPES.EMPTY)],
            [piece(COLORS.WHITE, SHAPES.EMPTY),piece(COLORS.WHITE, SHAPES.EMPTY),piece(COLORS.WHITE, SHAPES.EMPTY)]
        ]                                                                                                                                                                                        
        
    
    def display(self):
        

       
        lines = []
        for y in range(len(self.board)):
            line = ''
            for x in range(len(self.board[y])):
                if self.board[y][x] is not None:
                    line = line + colored(self.board[y][x].shape + ' ', self.board[y][x].color)
                else:
                    line = line + '  '

            lines.append(line)  

        line = ''.join([str(1 + i) + ' ' for i in range(len(self.board[0]))])
        lines.insert(0, line)
        lines.append(line)

        for i in range(0, len(lines)):
            i_display = str(i).zfill(2) if 0 < i < len(lines) - 1 else '  '
            print(i_display, lines[i], i_display)

 
    
    def check_NotEmpty(self,row,column):
        emptyPiece = piece(COLORS.WHITE,SHAPES.EMPTY)
        if self.board[row][column]!=emptyPiece :
            return True
        return False 
    
    def check_adjacent(self,row, column):
        adjs=[]
        if column==0 :
            if  self.check_NotEmpty(row,column+1):
                adjs.append(self.board[row][column+1])
            if row==0:
                if  self.check_NotEmpty(row+1,column):
                    adjs.append(self.board[row+1][column])
            elif row==len(self.board)-1:
                if  self.check_NotEmpty(row-1,column):
                    adjs.append(self.board[row-1][column])
            else:
                if  self.check_NotEmpty(row+1,column):
                    adjs.append(self.board[row+1][column])
                if  self.check_NotEmpty(row-1,column):
                    adjs.append(self.board[row-1][column])
        elif column==len(self.board[row])-1:
            if  self.check_NotEmpty(row,column-1):
                adjs.append(self.board[row][column-1])
            if row==0:
                if  self.check_NotEmpty(row+1,column):
                    adjs.append(self.board[row+1][column])
            elif row==len(self.board)-1:
                if  self.check_NotEmpty(row-1,column):
                    adjs.append(self.board[row-1][column])
            else:
                if  self.check_NotEmpty(row+1,column):
                    adjs.append(self.board[row+1][column])
                if  self.check_NotEmpty(row-1,column):
                    adjs.append(self.board[row-1][column])
        else:
            if row==0:
                if self.check_NotEmpty(row,column-1):
                    adjs.append(self.board[row][column-1])
                if self.check_NotEmpty(row,column+1):
                    adjs.append(self.board[row][column+1])
                if self.check_NotEmpty(row+1,column+1):
                    adjs.append(self.board[row+1][column+1])  
            elif row==len(self.board)-1:  
                if self.check_NotEmpty(row,column-1):
                    adjs.append(self.board[row][column-1])
                if self.check_NotEmpty(row,column+1):
                    adjs.append(self.board[row][column+1])
                if self.check_NotEmpty(row-1,column):
                    adjs.append(self.board[row-1][column])  
            else:
                if self.check_NotEmpty(row,column-1):
                    adjs.append(self.board[row][column-1])
                if self.check_NotEmpty(row,column+1):
                    adjs.append(self.board[row][column+1])
                if self.check_NotEmpty(row+1,column):
                    adjs.append(self.board[row+1][column])
                if self.check_NotEmpty(row-1,column):
                    adjs.append(self.board[row-1][column])
        return adjs
    
    def checkRange(self,row,column):
        if(column>len(self.board[row])-1 and column<0):
            return False
        elif(row>len(self.board)-1 and row<0):
            return False
        return True
    def checkPlay(self,row,column,tile):
        if not self.check_NotEmpty(row,column):
            adj=self.check_adjacent(row,column)
            if  len(adj)!=0:
                if self.check_ColorShape(tile,adj):
                    return True
    def check_ColorShape(self,tile,adjs):
        countColor=0
        countShape=0
        for i in adjs:
            if i.color==tile.color and i.shape!=tile.shape:
                countColor+=1                
            elif i.shape==tile.shape and i.color!=tile.color:
                countShape+=1
        if countColor!=0 or countShape!=0:
            if countColor==countShape or (countColor!=0 and countShape==0)or(countColor==0 and countShape!=0):
                return True    
        else:
            return False
    
       
    

