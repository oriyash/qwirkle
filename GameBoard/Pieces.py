class COLORS:
    RED = 'red'
    YELLOW = 'yellow'
    GREEN = 'green'
    CYAN = 'cyan'
    MAGENTA = 'magenta'
    BLUE = 'blue'
    WHITE = 'white'

class SHAPES:
    TRIANGLE = '▲'
    DIAMOND = '◆'
    SQUARE = '■'
    CIRCLE = '●'
    STAR = '★'
    SPARKLE = '❈'
    EMPTY = '☐'

class piece:
    def __init__(self,color,shape):
        self.color = color
        self.shape = shape

    def __call__(self, color, shape):
        self.color = color 
        self.shape = shape 
        return self

    def __eq__(self, other):
        if not isinstance(other, piece):
            return NotImplemented
        return self.color == other.color and self.shape == other.shape

    def __str__(self):
        return '{} {}'.format(self.color,self.shape)

    def __repr__(self):
        return self.__str__()

