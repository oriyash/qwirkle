# QWIRKLE PIB 1 - YASH ORI, GABRIEL ALBINO 

## Observation:
Works better on Linux enviroment because of the symbols used. 
Works on Windows also but some bugs may occur

## Requirements:
termcolor `$ pip3 install termcolor` or  `> python -m pip install termcolor`
colorama (windows only) `> python -m pip install colorama`

## Start game:
-->Use cd to navigate to root of project.
-->Start with <code>$ python3 main.py</code>
-->First input the amount of players (from 2 to 18)
-->Input the Name of each player
-->Set up the rules with 3 integers inputs. 
        The first is the amount of colors in game (3 to 6)
        The second is the amount of shapes in the game (3 to 6)
        And the last one is the amount of identical pieces (2 to n)
-->Select the first tile to play(input from 1 to 6) this tile is always added 
    at the center of the initial grid (2,2)
--> After the first tile on place you can select other tiles to add with the process of 
    selecting the tile from 1 to 6
--> Select the row and column where you want to put the tile
--> After finishing your turn type 0 on the console and its next player turn with the same process
## Middle of the game
--> Select the tile to add from your hand,selecting the tile from 1 to 6
--> Select the row and column where you want to put the tile (always put it inside the grid)
--> After finishing your turn type 0 on the console and its next player turn with the same process
## Trading hands
--> For trading tiles, at the start of your turn type -1
--> Input the amount of tiles to be traded
-->Input the position of the tile(s) you want to trade (1,6)
## Finish the game
--> the game finishes when there is no more tiles in the bag and the player clears his hand

## What is missing from the requirements:
--> AI to play against
--> Backup
-->The play validation function is incomplete(check only direct neighbors) and there is no score
