from Players import *
from GameBoard import *
from Functions import *
from random import randint
if os.name=='nt': 
    import colorama
    colorama.init()

# Player initialisation
numberOfPlayers = getInt('Number of players (between 2 and 18): ')
while numberOfPlayers<2 or numberOfPlayers>18:
    print(colored('Invalid number of players. Try Again.', COLORS.RED))
    numberOfPlayers = getInt('Number of players (between 2 and 18): ')

playerList = []

for i in range(numberOfPlayers):
    nameIn = input('What is the name of player {}: '.format(i+1)).upper().strip()
    while nameIn=='':
        nameIn = input('What is the name of player {}: '.format(i+1)).upper().strip()
    player = human(nameIn, i, 0)
    print('Player {} created succesfully!'.format(nameIn))
    playerList.append(player)
pause()

# Creates new bag of tiles
allTiles = setBag()
board = gameboard()

clearScreen()

whoPlays = randint(0, numberOfPlayers-1)
print(colored('{} STARTS!'.format(playerList[whoPlays].name),COLORS.RED))

giveTiles(playerList,allTiles)


# Main game loop
currentPlayer = playerList[whoPlays]

currentPlayer.showTiles()
board.display()
start(board.board,currentPlayer,allTiles)
while True:
    play(board,currentPlayer,allTiles)
    whoPlays = switchPlayer(numberOfPlayers, whoPlays)
    currentPlayer = playerList[whoPlays]
    if len(allTiles)==0 and len(playerList[whoPlays].tiles)==0:
        break