from termcolor import colored
from Functions import getInt
from random import randint

class human:

    def __init__(self, name, id, score):
        self.name = name
        self.id = id
        self.score = score
        self.tiles = []

    def __str__(self):
       return "{}'s score is {}".format(self.name,str(self.score))

    def scoreUp(self, up):
        self.score = self.score+up

    def addTile(self,tile):
        self.tiles.append(tile)

    def showTiles(self):
        out = ''
        for tile in self.tiles:
             out = out + colored(tile.shape, tile.color) +' '
        print("YOUR TILES: {}".format(out))
        print('            1 2 3 4 5 6')
    def trade_Hands(self,bag):
        if len(bag)>0:
            amount=getInt('Enter the number of the tiles you want to trade:') 
            tiles_T=[] 
            tileIndex = randint(0,len(bag)-1) 
            for n in range(amount):
                tiles_T.append(getInt('Enter the position of the tile to trade:')-1)
                self.tiles.append(bag[tileIndex])
                bag.pop(tileIndex)
            tiles_T.sort(reverse = True)
            for n in tiles_T:     
                bag.append(self.tiles[n])
                
                self.tiles.pop(n)
